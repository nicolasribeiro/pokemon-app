import Head from "next/head";
import { Navbar } from "../ui";

interface MainLayoutProps {
  title?: string;
  children: React.ReactNode;
}

export const MainLayout = ({ title, children }: MainLayoutProps) => {
  return (
    <>
      <Head>
        <title>{title ? title : "Pokemon App"}</title>
        <meta name="author" content="Nicolas Ribeiro" />
        <meta
          name="description"
          content="Informacion sobre los pokemon que mas te gusten!"
        />
        <meta name="keywords" content="pokemon, pokedex" />
      </Head>
      <Navbar />
      <main style={{ padding: "0 2rem" }}>{children}</main>
    </>
  );
};
