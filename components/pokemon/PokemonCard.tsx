import { SmallPokemon } from "@/interfaces";
import { Card, Row, Text } from "@nextui-org/react";
import { useRouter } from "next/router";

interface PokemonCardProps {
  pokemon: SmallPokemon;
}

export const PokemonCard = ({ pokemon }: PokemonCardProps) => {
  const router = useRouter();

  const handlePokemonClick = (): void => {
    router.push(`/name/${pokemon.name}`);
  };

  return (
    <Card isHoverable isPressable onClick={handlePokemonClick}>
      <Card.Body css={{ p: 1 }}>
        <Card.Image src={pokemon.img} width="100%" height={140} />
      </Card.Body>
      <Card.Footer>
        <Row justify="space-between">
          <Text transform="capitalize">{pokemon.name}</Text>
          <Text>#{pokemon.id}</Text>
        </Row>
      </Card.Footer>
    </Card>
  );
};
