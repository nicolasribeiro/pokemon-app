import { pokeApi } from "@/api";
import { MainLayout } from "@/components/layouts";
import { Grid } from "@nextui-org/react";
import { NextPage } from "next";
import { PokemonListResponse, SmallPokemon } from "../interfaces";
import { PokemonCard } from "@/components/pokemon";
import { Pagination } from "@nextui-org/react";
import { useCallback, useEffect, useState } from "react";

interface HomePageProps {
  pokemons: SmallPokemon[];
}

const HomePage: NextPage<HomePageProps> = () => {
  const [page, setPage] = useState<number>(1);
  const [pokemonList, setPokemonList] = useState<SmallPokemon[]>([]);
  const maxPage = 18;
  const itemsPerPage = 36;
  const offset = (page - 1) * itemsPerPage;

  const handleChangePage = (e: number): void => {
    setPage(e);
  };

  const callNewPageOfPokemon = useCallback(async () => {
    const { data } = await pokeApi.get<PokemonListResponse>(
      `pokemon?limit=${itemsPerPage}&offset=${offset}`
    );

    const newDataResults: SmallPokemon[] = data.results.map(
      (pokemon, index) => ({
        ...pokemon,
        id: index + 1 + offset,
        img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${
          index + 1 + offset
        }.svg`,
      })
    );

    setPokemonList(newDataResults);
  }, [offset]);

  useEffect(() => {
    callNewPageOfPokemon();
  }, [callNewPageOfPokemon]);

  return (
    <MainLayout>
      <Grid.Container gap={2} justify="flex-start">
        {pokemonList?.map(
          (pokemon, i) =>
            i + 1 < 37 && (
              <Grid key={i} xs={6} sm={3} md={2} xl={1}>
                <PokemonCard pokemon={pokemon} />
              </Grid>
            )
        )}
        <Pagination
          loop
          color="success"
          total={maxPage}
          initialPage={1}
          css={{ flexGrow: 1 }}
          onChange={handleChangePage}
        />
      </Grid.Container>
    </MainLayout>
  );
};

/**
getStaticProps se ejecutara 1 sola vez y es al momento de hacer el build.
sirve para cuando ya sabemos de antemano que el cliente necesitara esa pagina
por ejemplo, en este proyecto, mostraremos 151 pokemon y getStaticProps
creara 151 archivos html con su respectivo estilo para asi tener ya toda la pagina cargada.

getStaticProps sirve mucho en estos casos:
- The data required to render the page is available at build time ahead of a user’s request
- The data comes from a headless CMS
- The page must be pre-rendered (for SEO) and be very fast — getStaticProps generates HTML and JSON files, both of 
  which can be cached by a CDN for performance
-The data can be publicly cached (not user-specific). This condition can be bypassed in certain specific situation 
  by using a Middleware to rewrite the path.

Para mas informacion, visitar el siguiente link
https://nextjs.org/docs/pages/building-your-application/data-fetching/get-static-props
*/

/*
export const getStaticProps: GetStaticProps = async (ctx) => {

  const currentPage = parseInt((ctx.params?.page as string) || "18"); // Maximo 18 paginas para mostrar todas las imagenes
  const itemsPerPage = 36;
  const offset = (currentPage - 1) * itemsPerPage;

  const { data } = await pokeApi.get<PokemonListResponse>(
    `pokemon?limit=${itemsPerPage}&offset=${offset}`
  );

  const newDataResults: SmallPokemon[] = data.results.map((pokemon, index) => ({
    ...pokemon,
    id: index + offset,
    img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${
      index + 1 + offset
    }.svg`,
  }));

  return {
    props: {
      pokemons: newDataResults,
    },
  };
};
*/
export default HomePage;
