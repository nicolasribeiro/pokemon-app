import { MainLayout } from "@/components/layouts";
import { PokemonCard } from "@/components/pokemon";
import { NoFavorite } from "@/components/ui";
import { SmallPokemon } from "@/interfaces";
import { ToggleFavoriteProps, pokemons } from "@/utils";
import { Container, Grid, Text, Image, Card, Row } from "@nextui-org/react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const FavoritesPage: NextPage = () => {
  const [favoritePokemons, setFavoritePokemons] = useState<
    ToggleFavoriteProps[]
  >([]);
  const router = useRouter();

  const handlePokemonClick = (name: string): void => {
    router.push(`/name/${name}`);
  };

  useEffect(() => {
    setFavoritePokemons(pokemons());
  }, []);

  return (
    <MainLayout title="Pokemon - Lista de Favoritos">
      <Container>
        <Text h2>Lista de favoritos</Text>
      </Container>
      <Grid.Container>
        {favoritePokemons.length === 0 ? (
          <NoFavorite />
        ) : (
          favoritePokemons?.map((pokemon: ToggleFavoriteProps) => (
            <Grid
              key={pokemon.id}
              xs={12}
              sm={6}
              md={3}
              xl={1}
              css={{ marginLeft: "1rem" }}
            >
              <Card
                isHoverable
                isPressable
                onPress={() => handlePokemonClick(pokemon.pokeName)}
              >
                <Card.Body css={{ p: 1 }}>
                  <Card.Image
                    src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemon.id}.svg`}
                    width="100%"
                    height={140}
                  />
                </Card.Body>
                <Card.Footer>
                  <Row justify="space-between">
                    <Text transform="capitalize">Mas Informacion</Text>
                    <Text>#{pokemon.id}</Text>
                  </Row>
                </Card.Footer>
              </Card>
            </Grid>
          ))
        )}
      </Grid.Container>
    </MainLayout>
  );
};

export default FavoritesPage;
