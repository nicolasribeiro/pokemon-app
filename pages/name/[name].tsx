import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import { MainLayout } from "@/components/layouts/MainLayout";
import { Grid, Image, Text } from "@nextui-org/react";
import { Card } from "@nextui-org/react";
import { Container } from "@nextui-org/react";
import { Button } from "@nextui-org/react";
import { PokemonDetail, PokemonListResponse, SmallPokemon } from "@/interfaces";
import { useEffect, useState } from "react";
import { existInFavorites, toggleFavorite } from "@/utils";
import conffetti from "canvas-confetti";
import { pokeApi } from "@/api";

interface PokemonByNameProps {
  pokemon: PokemonDetail;
}

const PokemonByName: NextPage<PokemonByNameProps> = ({ pokemon }) => {
  const [isInFavorites, setIsInFavorites] = useState<boolean>(false);

  const handleSaveFavorite = (): void => {
    toggleFavorite({ pokeName: pokemon?.name, id: pokemon.id });
    setIsInFavorites(
      existInFavorites({ pokeName: pokemon?.name, id: pokemon.id })
    );

    if (isInFavorites) return;

    conffetti({
      zIndex: 999,
      particleCount: 100,
      spread: 160,
      angle: -100,
      origin: {
        x: 1,
        y: 0,
      },
    });
  };

  useEffect(() => {
    setIsInFavorites(
      existInFavorites({ pokeName: pokemon?.name, id: pokemon.id })
    );
  }, [pokemon]);

  return (
    <MainLayout title={`${pokemon?.name?.toUpperCase()} - PokeApp`}>
      <Grid.Container css={{ marginTop: "5px" }} gap={2}>
        <Grid xs={12} sm={4}>
          <Card isHoverable css={{ padding: "30px" }}>
            <Card.Body>
              <Card.Image
                src={
                  pokemon?.sprites.other?.dream_world.front_default ||
                  "/no-image.png"
                }
                alt={pokemon?.name}
                width="100%"
                height={200}
              />
            </Card.Body>
          </Card>
        </Grid>

        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header
              css={{ display: "flex", justifyContent: "space-between" }}
            >
              <Text h1 transform="capitalize">
                {pokemon?.name}
              </Text>
              <Button
                color={"gradient"}
                ghost={!isInFavorites}
                onClick={handleSaveFavorite}
              >
                {isInFavorites
                  ? "Remover de Favoritos"
                  : "Guardar en Favoritos"}
              </Button>
            </Card.Header>
            <Card.Body>
              <Text size={30}>Sprites</Text>
              <Container direction="row" display="flex" gap={0}>
                <Image
                  src={pokemon?.sprites.front_default}
                  alt={pokemon?.name}
                  width={120}
                  height={120}
                />

                <Image
                  src={pokemon?.sprites.back_default}
                  alt={pokemon?.name}
                  width={100}
                  height={100}
                />

                <Image
                  src={pokemon?.sprites.front_shiny}
                  alt={pokemon?.name}
                  width={100}
                  height={100}
                />

                <Image
                  src={pokemon?.sprites.back_shiny}
                  alt={pokemon?.name}
                  width={100}
                  height={100}
                />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </MainLayout>
  );
};

export default PokemonByName;

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await pokeApi.get<PokemonListResponse>("/pokemon?limit=151");

  return {
    paths: data?.results.map(({ name }: SmallPokemon) => ({
      params: { name },
    })),
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    const { name } = params as { name: string };
    const { data } = await pokeApi.get<PokemonDetail>(`/pokemon/${name}`);

    return {
      props: {
        pokemon: data,
      },
      revalidate: 86400,
    };
  } catch (error) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
};
