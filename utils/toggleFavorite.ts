
export interface ToggleFavoriteProps{
  id: number;
  pokeName: string;
}



const toggleFavorite = (pokemon: ToggleFavoriteProps): void => {
  const storedFavorites: string | null = localStorage.getItem('pokemonFavList')
  let favorites: ToggleFavoriteProps[] = storedFavorites ? JSON.parse(storedFavorites) : []

  const isPokemonInFavorites = favorites.some(
    pkm => pkm.pokeName === pokemon.pokeName && pkm.id === pokemon.id
  )

  if (isPokemonInFavorites) {
    favorites = favorites.filter(
      pkm => !(pkm.pokeName === pokemon.pokeName && pkm.id === pokemon.id)
    )
  } else {
    favorites.push(pokemon)
  }

  localStorage.setItem('pokemonFavList', JSON.stringify(favorites))

}


const existInFavorites = (pokemon: ToggleFavoriteProps): boolean => {
  const favorites: ToggleFavoriteProps[] = JSON.parse(localStorage.getItem('pokemonFavList') || '[]')
  return favorites.some(fav => fav.pokeName === pokemon.pokeName && fav.id === pokemon.id)
}

const pokemons = (): ToggleFavoriteProps[] => JSON.parse(localStorage.getItem('pokemonFavList') || '[]')


export {
  existInFavorites,
  toggleFavorite,
  pokemons
}